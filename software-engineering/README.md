# Guideline Engenharia de Software

## Modelos de banco de dados

- O programa utilizado para gerar os modelos de banco de dados (arquivos .mwb) é o MySQL Workbench
- Link para download do MySQL Workbench: <https://www.mysql.com/products/workbench/>


## Prototipagem das telas

- A prototipagem foi realizada por meio de templates html
- No diretório prototype/ estão os protótipos para o site e para o admin


## Documentos de Engenharia de Software

- Os documentos (diagramas, requisitos, uml, etc...) encontram-se no diretório documents/


## Metodologia Ágil (Adminstração de tarefas com Kanban)

- Todos os nossos cards e tarefas estão cadastradas no trello: https://trello.com/b/6tNXQxrr/uniplacdigital


## Site On-line

O site está publicado em: http://welison.pythonanywhere.com/

O host que conseguimos é free, por tanto, a conexão com o banco de dados é instável. Caso entre na página de erro 500, clique em voltar para o site, pois as vezes o site perde a conexão brevemente.


## Usuários para testes:

Foram cadastrados 4 usuários, cada qual com um nível de permissão. Estes são:

- login: 111111, senha: 123456, permissão: Administrador (Nível 1);
- login: 222222, senha: 123456, permissão: Editor (Nível 2);
- login: 333333, senha: 123456, permissão: Autor (Nível 3);
- login: 444444, senha: 123456, permissão: Usuário (Nível 4)


#### Qualquer dúvida, estamos à disposição.